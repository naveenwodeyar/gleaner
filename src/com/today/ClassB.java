package com.today;

import java.util.*;
import java.util.stream.Collectors;

public class ClassB
{
    public static void main(String[] args)
    {
        List<Integer> numList = Arrays.asList(1,4,7,5,2,6,8,4,3);

        /** **/
        // 1. Partitioning a list: partitioningBy(),
                System.out.println("\n Partitioning a list: partitioningBy()");
                numList.parallelStream().collect(Collectors.partitioningBy(n -> n==2)).entrySet().forEach(System.out::println);

        // 2. Joining elements: joining(),
            List<String> names = List.of("One","Two","Three","Four","Five","Six","Seven");
            System.out.println("\n Joining the string");
            var st = names.stream().collect(Collectors.joining());
            System.out.println(st);

            var st1 = names.parallelStream().collect(Collectors.joining(",", "{", "}"));
            System.out.println(st1);

        // 3. Averaging long: averagingLong(), Finds the average value of a collection of long values.
            System.out.println("\n averagingLong(),");
            var avgInt = numList.parallelStream().collect(Collectors.averagingLong(Integer::longValue));
            System.out.println(avgInt);
            System.out.println("\n ----------");
            var avgInt1 = numList.parallelStream().collect(Collectors.averagingLong(n -> n ));
            System.out.println(avgInt1);



    }
}

package com.today;
import java.util.*;
import java.util.stream.Collectors;

public class ClassC
{
    public static void main(String[] args)
    {
        List<Integer> list = List.of(1,4,6,8,0,43,45);
            System.out.println("/n======================\n");

        // 1. Creating Map: toMap(), Creates a map from the values of a collection.
            System.out.println("\n Collectors.toMap()");
                list.parallelStream()
                        .collect(Collectors.toMap(Integer::intValue,Integer::longValue))
                        .entrySet()
                        .forEach(System.out::println);
    }
}

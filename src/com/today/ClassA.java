package com.today;

import java.util.*;
import java.util.stream.Collectors;

public class ClassA
{
    public static void main(String[] args)
    {
        List<Integer> numList = List.of(8,4,98,1,65,7,46,2,65);

        /**
        // 1. Finding minimum value: minBy(),
            System.out.println("\n Minimum value in the list,");
            var min = numList.stream().min(Comparator.comparing(Integer::intValue)).get();
            System.out.println(min);

            System.out.println("\n ************");
            var min1 = numList.stream().sorted(Comparator.reverseOrder()).findFirst().get();

            System.out.println("\n ************");
                numList.stream().collect(Collectors.minBy(Comparator.reverseOrder())).ifPresent(System.out::println);

        **/
        // 2. Finding maximum value: maxBy(),
            System.out.println("\n Maximum valur in the list,");
//            var parallel = numList.stream().isParallel();
//             System.out.println(parallel);
                var max = numList.parallelStream().max(Comparator.comparing(Integer::intValue)).get();
                System.out.println(max);
                System.out.println("\n ---------------\n");
                 var max1 = numList.parallelStream().sorted(Comparator.reverseOrder()).findFirst().get();
                System.out.println(max1);
                System.out.println("\n ---------------");
                numList.parallelStream().collect(Collectors.maxBy(Comparator.naturalOrder())).ifPresent(System.out::println);
                System.out.println(max1);

    }
}

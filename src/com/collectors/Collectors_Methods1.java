package com.collectors;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Collectors_Methods1
{
    public static void main(String[] collectors)
    {
        /**
        // 1.a) Finding minimum value: minBy(), It will return the minimum value present in a list.
            System.out.println("\n******************");
            List<Integer> numList5 = List.of(1,2,3,4,5,6,7,8,9,10,1,8,5,3,6,7);
                var minInt = numList5.stream().collect(Collectors.minBy(Comparator.naturalOrder())).get();
                    System.out.println("\nMinimum valued number in the list="+minInt);

        // 1.b)
            var min = numList5.parallelStream().min(Comparator.comparing(Integer::intValue));
                System.out.println("\nMinimum value in the list using Comparator="+min.get());

        // 1.c)
            var max = numList5.parallelStream().collect(Collectors.minBy(Comparator.reverseOrder()));
                 System.out.println("\nMaximum value in the list="+max.get());

         **/
        // 2. Finding maximum value: maxBy()
            System.out.println("\n******************");
            List<Integer> numList2 = List.of(1,2,3,4,5,6,7,8,9,10,1,8,5,3,6,7);
            var max = numList2.parallelStream().collect(Collectors.maxBy(Comparator.naturalOrder()));
        System.out.println("\n Maximum valued integer in the list="+max.get()+":"+max.getClass());

    }
}


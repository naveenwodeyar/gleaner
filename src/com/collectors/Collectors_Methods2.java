package com.collectors;

import java.util.*;
import java.util.stream.Collectors;

public class Collectors_Methods2
{
    public static void main(String[] collectors)
    {
        /**
        // 1.  Partitioning a list: partitioningBy()
            System.out.println("\n******************");
            List<Integer> numList = List.of(1,2,3,4,5,6,7,8,9,10,1,8,5,3,6,7);
                var values = numList.parallelStream().collect(Collectors.partitioningBy(n -> n > 2));
                 System.out.println(values);
             System.out.println("\n **************** \n");
             List<String> strings = Arrays.asList("a","alpha","beta","gamma");
             strings.parallelStream().collect(Collectors.partitioningBy(st-> st.length() >= 3)).entrySet().forEach(System.out::println);

         **/
        // 2. Creating unmodifiable list: toUnmodifiableList(), It is used to create a read-only list. Any try to make a change in this unmodifiable list will result in UnsupportedOperationException.
            System.out.println("\n******************");
            List<Integer> numList1 = List.of(1,2,3,4,5,6,7,8,9,10,1,8,5,3,6,7);
                 var unModifiableSet = numList1.stream().collect(Collectors.toUnmodifiableSet());
                     System.out.println(unModifiableSet);
//                unModifiableSet.add(0);    exception occurs,


    }
}


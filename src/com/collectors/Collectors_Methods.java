package com.collectors;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Collectors_Methods
{
    static
    {
        System.out.println("\n Collectors is a final class that extends the Object class. " +
                "\n It provides reduction operations, such as accumulating elements into collections, " +
                "\n summarizing elements according to various criteria, etc");
    }

    public static void main(String[] collectors)
    {
        /**
        // 1. Creating list: toList(), It is used to accumulate elements into a list. It will create a new list (It will not change the current list).
                System.out.println("\n******************");
                List<Integer> numList = List.of(1,2,3,4,5,6,7,8,9,0);
                 for (Integer i : numList.stream().collect(Collectors.toList()))
                 {
                     System.out.println(i);
                }

        // 2.Creating set: toSet(), It is used to accumulate elements into a set, It will remove all the duplicate entries.
             System.out.println("\n******************");
             List<Integer> numList1 = List.of(1,2,3,4,5,6,7,8,9,0,1,8,5,3,6,7);
                 for(Integer i: numList1.parallelStream().collect(Collectors.toSet()))
                 {
                     System.out.println(i);
                 }

        // 3.Creating specific collection: toCollection(), We can accumulate data in any specific collection as well.
            System.out.println("\n******************");
            List<Integer> numList2 = List.of(1,2,3,4,5,6,7,8,9,0,1,8,5,3,6,7);
//                for (Integer i : numList2.parallelStream().toList())
                for (Integer i : numList2.parallelStream().collect(Collectors.toCollection(HashSet::new)))
                {
                    System.out.println(i);
                }

        // 4.Counting elements: Counting(), It will return the number of elements present in the computed collection.
                System.out.println("\n******************");
                List<Integer> numList4 = List.of(1,2,3,4,5,6,7,8,9,0,1,8,5,3,6,7);
                    System.out.println(numList4.parallelStream().count());


        **/
        // 5.Finding minimum value: minBy(), It will return the minimum value present in a list.
            System.out.println("\n******************");
            List<Integer> numList5 = List.of(1,2,3,4,5,6,7,8,9,10,1,8,5,3,6,7,8,9);
                numList5.parallelStream().min(Comparator.comparing(Integer::intValue)).ifPresent(System.out::println);
                var i = numList5.stream().collect(Collectors.minBy(Comparator.naturalOrder())).get().intValue();
                System.out.println("Minimum value in the give list:"+i);
    }
}


package com.exceptn;

public class CheckedExcptn
{
    static
    {
        System.out.println("Exception is an event that disrupts the normal flow of the program." +
                " \nIt is an object which is thrown at runtime.");
        System.out.println("\n Exceptions under the parent class java.lang.Throwable");
    }

    // 1.
    static void checkedExcpn(int a)
    {
        System.out.println("\n Exceptions that are checked at compile time(Checked by compiler)" +
                "\n 1. IOException" +
                "\n 2. SQLException" +
                "\n 3. FileNotFoundException" +
                "\n 4. ClassNotFoundException " +
                "\n 5. IllegalAccessException " +
                "\n 6. InstantiationException " +
                "\n 7. NoSuchFieldException " +
                "\n 8. NoSuchMethodException " +
                "\n 9. CloneNotSupportedException " +
                "\n 10. InterruptedException "
        );
    }

    // 2/
    static void unCheckedExcptn(float b)
    {
        System.out.println("\n Exceptions that are checked at the run time." +
                "\n1. ArithmeticException, \n2. NullPointerException, \n3. ArrayIndexOutOfBoundsException");
    }

    // 3.
    static void erroR(long l)
    {
        System.out.println("\n Error is irrecoverable." +
                "\n 1. OutOfMemoryError, \n 2.VirtualMachineError, \n 3.AssertionError etc.");
    }

    public static void main(String[] args)
    {
        System.out.println("\n Exception Handling is a mechanism to handle runtime errors such as ClassNotFoundException, " +
                "\nIOException, \nSQLException, \nRemoteException, etc.");


    }

}

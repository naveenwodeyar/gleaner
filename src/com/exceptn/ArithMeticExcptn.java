package com.exceptn;

public class ArithMeticExcptn
{
    static void arithMeticExcptn()
    {
        int a = 5;
        int b = 0;
        try
        {
            int c = a/b;
            System.out.println(c);
        }
        catch (ArithmeticException e)
        {
            System.out.println(e.getLocalizedMessage());
        }
        finally
        {
            System.out.println("Please check your code,");
            b = 5;
            System.out.println(a/b);
        }
    }
    public static void main(String[] args)
    {
        System.out.println("\n ArithmeticException!");
        arithMeticExcptn();
    }
}

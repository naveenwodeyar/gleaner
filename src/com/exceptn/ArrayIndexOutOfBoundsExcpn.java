package com.exceptn;

public class ArrayIndexOutOfBoundsExcpn
{
    static void arrayIndexExcpn(int[] arr)
    {
        System.out.println("\n********************\n");
        System.out.println("Array size:"+arr.length);
        try
        {
            System.out.println(arr[40]);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public static void main(String[] args)
    {
        System.out.println("/n When an array exceeds to it's size, the ArrayIndexOutOfBoundsException occurs.");
        arrayIndexExcpn(new int[]{1,5,9,8,7,5,3});
    }
}

package com.exceptn;

public class NullPtrExcptn
{
    // NullPointer Exception,
    static void nullPtrExcptn(String st)
    {
        try
        {
            System.out.println(st.length());
        }
        catch (NullPointerException e)
        {
            System.out.println(e.getLocalizedMessage());
        }

    }

    public static void main(String[] args)
    {
        System.out.println("\n NullPointerException.");
//        nullPtrExcptn("String");
        String st = null;

        nullPtrExcptn(st);
    }
}
